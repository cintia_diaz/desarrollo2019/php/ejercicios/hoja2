<?php
if ($_REQUEST){
    $mal = false;
} else {
    $mal = true;
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        if($mal) {
            ?>
            <form name="f">
                <select name="ciudad">
                    <option value="SS">Santander</option>
                    <option value="PA">Palencia</option>
                    <option value="PO">Potes</option>
                </select>
                <input type="submit" value="Enviar" name="boton" />
            </form>
            <?php
            } else {
                /* cuando selecciona una ciudad me indica el nombre completo
                 * de esta y no los codigos
                 */
                //var_dump($_REQUEST);
                $ciudades=[
                    "SS" => "Santander",
                    "PA" => "Palencia",
                    "PO" => "Potes"
                ];
                echo $ciudades[$_REQUEST['ciudad']];
            }
        ?>
    </body>
</html>
