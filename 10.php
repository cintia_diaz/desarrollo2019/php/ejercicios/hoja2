<?php
if($_REQUEST){
    $mal = false;
} else {
    $mal = true;
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        if ($mal) {
            ?>
            <form name="f">
                <label>
                    Seleccione la ciudad
                    <select multiple name="ciudad[]" size="15">
                        <optgroup label="Asia">
                            <option value="3">Delhi</option>
                            <option value="4">Hong Kong</option>
                            <option value="8">Mumbai</option>
                            <option value="11">Tokio</option>
                        </optgroup>
                        <optgroup label="Europe">
                            <option value="1">Amsterdam</option>
                            <option value="5">London</option>
                            <option value="7">Moscow</option>
                        </optgroup>
                        <optgroup label="North America">
                            <option value="6">Los Angeles</option>
                            <option value="9">New York</option>
                        </optgroup>
                        <optgroup label="South America">
                            <option value="2">Buenos Aires</option>
                            <option value="10">Sao Paulo</option>
                        </optgroup>
                    </select>
                    <input type="submit" value="Enviar" name="boton" />
                </label>
            </form>
            <?php
        } else {
            $ciudades =[
                "","Amsterdam", "Buenos Aires",
                "Delhi", "Hong Kong", "London",
                "Los Angeles", "Moscow", "Mumbai",
                "New York","Sao Paulo", "Tokyo"
            ];
            echo "Los elementos seleccionados son:" . "<br>";
            foreach ($_REQUEST['ciudad'] as $valor) {
                echo $valor . "-" . $ciudades[$valor] . "<br>";
            }         
            
            /* selecciona varios elementos y nos los muestra */
        }
        ?>
    </body>
</html>
