<?php 
if ($_REQUEST) {
    $mal = false;
} else {
    $mal = true;
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link type="text/css" rel="stylesheet" href="11.css">
    </head>
    <body>
        <div class="contenedor">
        <h1>Formulario de inscripción de usuarios</h1>
        <?php
        if ($mal) {
        ?>
        <div class="formulario">
        <form name="f">
            <div class="fila">
                <div class="col-1">
                    <label for="nombre">Nombre Completo</label>
                </div>
                <div class="col-2">
                    <input type="text" name="nombre" id="nombre">   
                </div>
            </div>            
            <div class="fila">
                <div class="col-1">
                    <label for="direccion">Direccion</label>
                </div>
                <div class="col-2">
                    <textarea name="direccion" id="direccion"></textarea>    
                </div>
            </div>           
            <div class="fila">
                <div class="col-1">
                    <label for="correo">Correo<br> Electronico</label>
                </div>
                <div class="col-2">
                    <input type="email" name="correo" id="correo">
                </div>
            </div>            
            <div class="fila">
                <div class="col-1">
                    <label for="cont">Contraseña</label>
                </div>
                <div class="col-2">
                    <input type="password" name="cont" id="cont">  
                </div>
            </div>            
            <div class="fila">
                <div class="col-1" id="confirmar">
                    <label for="confirmar">Confirmar<br> Contraseña</label>
                </div>
                <div class="col-2" id="confirmar">
                    <input type="password" name="cont" id="confirmar">
                </div>
            </div>           	
           	<div class="fila">
                <div class="col-1">
                <label for="fecha">Fecha de<br> Nacimiento</label>
                </div>
                <div class="col-2">
                    <select name="mes" id="fecha">
                        <option value="1">Enero</option>
                        <option value="2">Febrero</option>
                        <option value="3">Marzo</option>
                        <option value="4">Abril</option>
                        <option value="5">Mayo</option>
                        <option value="6">Junio</option>
                        <option value="7">Julio</option>
                        <option value="8">Agosto</option>
                        <option value="9">Septiembre</option>
                        <option value="10">Octubre</option>
                        <option value="11">Noviembre</option>
                        <option value="12">Diciembre</option>                    
                    </select>
                    <select name="dia">
                        <option value="1">01</option>
                        <option value="2">02</option>
                        <option value="3">03</option>
                        <option value="4">04</option>
                        <option value="5">05</option>
                        <option value="6">06</option>
                        <option value="7">07</option>
                        <option value="8">08</option>
                        <option value="9">09</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option>
                        <option value="13">13</option>
                        <option value="14">14</option>
                        <option value="15">15</option>
                        <option value="16">16</option>
                        <option value="17">17</option>
                        <option value="18">18</option>
                        <option value="19">19</option>
                        <option value="20">20</option>
                        <option value="21">21</option>
                        <option value="22">22</option>
                        <option value="23">23</option>
                        <option value="24">24</option>
                        <option value="25">25</option>
                        <option value="26">26</option>
                        <option value="27">27</option>
                        <option value="28">28</option>
                        <option value="29">29</option>
                        <option value="30">30</option>
                        <option value="31">31</option>
                    </select>
                    <input type="text" name="anio" class="anio">
                </div>    
            </div>            
            <div class="fila">
                <div class="col-1" id="sexo">
                    <label for="sexo">Sexo</label>  
                </div>
                <div class="col-2" id="sexo">
                    <input type="radio" name="sexo" value="H" id="hombre" >
                    <label for="hombre">Hombre</label>
                    <input type="radio" name="sexo" value="M" id="mujer" >
                    <label for="mujer">Mujer</label>  
                </div>
                <br>
            </div>
            <div class="fila">
                <div class="col-1">
                    <label for="intereses">Por favor elige<br> los temas de tus<br> intereses</label>
                </div>
                <div class="col-2" id="intereses">
                    <input type="checkbox" name="interes[]" value="1" id="ficcion">
                    <label for="ficcion">Ficcion</label> <br>                                  
                    <input type="checkbox" name="interes[]" value="2" id="accion">
                    <label for="accion">Acción</label>  <br>                  
                    <input type="checkbox" name="interes[]" value="3" id="suspense">
                    <label for="suspense">Suspense</label>   <br>                                
                    <input type="checkbox" name="interes[]" value="4" id="terror">
                    <label for="terror">Terror</label>	<br>
                    <input type="checkbox" name="interes[]" value="5" id="comedia">
                    <label for="comedia">Comedia</label>               
                </div>
            </div>            
            <div class="fila">
                <div class="col-1">
                    <label for="aficiones">Selecciona tus<br> aficiones <br><br>
                        (Selecciona multiples<br> elementos pulsando la<br> tecla Control y<br> haciendo clic en cada<br> uno, uno a uno)
                    </label>
                </div>
                <div class="col-2">
                    <select size="6" multiple="" name="aficion[]" id="aficiones">
                        <option value="1">Deportes al aire libre</option>
                        <option value="2">Deportes de aventuras</option>
                        <option value="3">Musica Pop</option>
                        <option value="4">Musica Rock</option>
                        <option value="5">Musica alternativa</option>
                        <option value="6">Fotografia</option>
                    </select>  
                </div>
            </div>
            <br>
            <div class="fila">
          		<input type="submit" value="Enviar" name="boton">              	
            </div>
        </form>
        </div>
        <?php
        } else {
            $dias=[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31];         
            $mes=['','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
            $sexo=[
                "H"=>'hombre',
                "M"=>'mujer'];
            $intereses=['','ficcion','accion','suspense','terror','comedia'];
            $aficiones=['','deportes al aire libre','deportes de aventuras','musica pop','musica rock','musica alternativa','fotografia'];
            echo 'Su nombre es ' . $_REQUEST['nombre'];
            echo '<br>';
            echo 'Su direccion es ' . $_REQUEST['direccion'];
            echo '<br>';
            echo 'Su correo electronico es '. $_REQUEST['correo'];
            echo '<br>';
            echo 'Su fecha de nacimiento es ' . $dias[$_REQUEST['dia']] . 
                    '-' . $mes[$_REQUEST['mes']] . '-' . $_REQUEST['anio'];
            echo '<br>';
            echo 'Usted es ' . $sexo[$_REQUEST['sexo']];
            echo '<br>';
            echo 'Los temas de su interes son ';
            foreach ($_REQUEST['interes'] as $value) {
                echo "$intereses[$value]" . ", ";
            }
            echo '<br>';
            echo 'Sus aficiones son ' ;
            foreach ($_REQUEST['aficion'] as $value) {
                echo "$aficiones[$value]" . ", "; 
            }
                    
                    
        }
        ?>
            </div>
    </body>
</html>
