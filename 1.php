<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $url = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
        // concatena para devolver en url la dirección donde se encuentra el archivo
        $posicion = strrpos($url, "/");
        // strrpos: encuentra la posicion de la ultima aparación de "/" en el string $url
        $path = substr($url, 0, $posicion);
        // substr: devuelve parte de una cadena
        // toma desde la posicion hasta 0 hasta la posicion calculada antes de la ultima "/" en la cadena $url
        echo("Location: $path" . "/destino.php?error='Introduce los datos necesarios'");
        // Te devuelve la nueva direccion con el $path concatenada con el nuevo string
        ?>
    </body>
</html>
